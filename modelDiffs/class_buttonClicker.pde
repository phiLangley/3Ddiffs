////this is a class to create a click-able button, used to display the model or the graph
class buttonClicker{
  int index;
  int posX=0;
  int posY = height - guiDim;
  int dimX=(screenW/3);
  int dimY=20;
 
  boolean active=true;
  
  buttonClicker(int i){
    index=i;
    posX=(screenW/3)*i;
  }
  
  void update(){
    if ((mouseY>screenH + (screenGUI/2))&&(mouseX>posX)&&(mouseX<posX+dimX)){
     if (active==false){
        active=true;
      } else {
        active=false;
      }  
    }
  }
  
  void display(){
    
    stroke(0);
    strokeWeight(1);
    
    if (active==true){
      fill(255);
      rect(posX,posY,dimX, dimY);
    } else {
      fill(50);
      rect(posX,posY,dimX, dimY);
    }
    
    pixMod[index].dispMesh=active;
    pixMod[index].dispGraph=!active;

  }
  
}

//////////////////////////////////////////////////////////////////
void initializeButtons(){
  for (int i=0; i<3; i++){
    buttonClick[i] = new buttonClicker(i);
    if (i!=1){ buttonClick[i].active=false;}
  }
}


