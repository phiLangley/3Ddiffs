////this sis a class to create an array of buttons to select the active file
class buttonSlider{
  int index;
  int posX=0;
  int posY=0;
  int dimX=(screenW/3);
  int dimY=guiDim;
  int activeVal=0;
  int activeIndex=0;
  String activeFile;
  int sepWidth=0;
  
  buttonSlider(int i){
    index=i;
    posX=(screenW/3)*i;
    activeFile = imageList.get(activeIndex);
  }
  
  void update(){
    if ((mouseY<screenGUI)&&(mouseX>posX)&&(mouseX<posX+dimX)){
      activeVal = mouseX-posX;
      activeIndex = int(map(activeVal, 0, screenW/3, 0, imageList.size()));
      activeFile = imageList.get(activeIndex);
      println(index);
      println(activeFile);
//      println(activeIndex);
    }
  }
  
  void display(){
         
    fill(255);
    strokeWeight(1);
    stroke(0);
    rect(posX,posY,dimX, dimY);
    
    for (int i=0; i<imageList.size(); i++){
      if (activeIndex>=i){
        fill(255);
      } else {
        fill(50);
      }
      strokeWeight(1);
      stroke(0);
      rect(posX+(i*((screenW/3)/imageList.size())),posY,dimX, dimY);
    }
    
    fill(255);
    textAlign(LEFT, TOP);
    textSize(guiDim/2);
    text(activeFile, posX, screenGUI/4); 
  }
  
}

//////////////////////////////////////////////////////////////////
void initializeSliders(){
  for (int i=0; i<3; i++){
    buttonSlide[i] = new buttonSlider(i);
  }
}


