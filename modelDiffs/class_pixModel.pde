////a class for each file, loading the pixel data and the display of the graph and the model
class pixModel { 

  int liveIndex=1;
  int index;
  
  int minX = 0;
  int maxX = -16777216;
  int minY = 0;
  int maxY = -16777216;
  int minZ = 0;
  int maxZ = -16777216;
  
  float xRange, yRange, zRange;
  
  int[] boundX = new int[2];
  int[] boundY = new int[2];
  int[] boundZ = new int[2];
  int[] boundMax = new int[2];
  
  float scaleFactor;
 
  PImage pixData;
  PGraphics pg, pgFace, pgVert;
  
  int maxWidth, maxWidthTemp;
  
  String [] pixRowArray = new String [graphHeight];
  String [] vertRowArray = new String [graphHeight];
  int[]  pixRowStatus = new int [graphHeight];
  
  ArrayList<String> fList, fListMap;
  ArrayList<Integer> pixList;
  
  int[] [] matchInd = new int [3][graphHeight];
  boolean[] [] makeCurve = new boolean [3][graphHeight];

  boolean dispMesh, dispGraph;

  pixModel(int ind, boolean graph, boolean mesh) {
    
    index = ind;
    dispGraph = graph;
    dispMesh = mesh;
    
    fList = new ArrayList<String>();
    fListMap = new ArrayList<String>();
    pixList = new ArrayList<Integer>();
    
  }
  
  void getDiffStatus() {

    for (int y=0; y<graphHeight ;y++){
      pixRowStatus[y]=0;      
      for (int n=0; n<3; n+=2){
        for( int yy=0; yy<graphHeight ;yy++){  
          makeCurve[n][y]=false;
          matchInd[n][y]=0;
          if ((pixRowArray[y].equals(pixMod[n].pixRowArray[yy])==true)&&(pixRowArray[y].equals("#")==false)) {
            matchInd[n][y]=yy;
            makeCurve[n][y]=true;
            pixRowStatus[y]+=(n+1);
            break;
          }
        }
      }
    }
    
  }
  
  void displayDiffMesh() {

    if (dispMesh==true) {

      float[] colPosA = new float[3];
      float[] colPosB = new float[3];
      float[] colPosC = new float[3];

      int faceCount=0;
      int matchIndex;   

      directionalLight(125, 125, 125, 0, 1, -1);
      ambientLight(175, 175, 175);
      strokeWeight(1/SCALE);
      stroke(0, 20); 
      fill(255,0,0);

      for (int y=0; y<pixRowArray.length; y++) {
        
        fill(255);
    
//////////////////////////////////////////////////////////CHECK THE DIFF STATUS HERE
        if (index==1) {
          if (pixRowStatus[y]==0) {
            fill(255, 0, 255);
          }
          if (pixRowStatus[y]==1) {
            fill(255, 255, 0);
          }
          if (pixRowStatus[y]==3) {
            fill(0, 255, 255);
          }
          if (pixRowStatus[y]==4) {
            fill(255, 255, 255);
          }
        } else {
          fill(255);
        }
//////////////////////////////////////////////////////////CHECK THE DIFF STATUS HERE
  
        String[] tempFace = splitTokens(pixRowArray[y], "#");   
                   
        for (int x=0; x<tempFace.length; x++) { 
          
          String[] tempPt = splitTokens(tempFace[x], ",");
 
          colPosA[0]= map(float(tempPt[0]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          colPosA[1]= map(float(tempPt[1]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          colPosA[2]= map(float(tempPt[2]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);

          colPosB[0]= map(float(tempPt[3]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          colPosB[1]= map(float(tempPt[4]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          colPosB[2]= map(float(tempPt[5]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);

          colPosC[0]= map(float(tempPt[6]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          colPosC[1]= map(float(tempPt[7]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          colPosC[2]= map(float(tempPt[8]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
          
            beginShape();
              vertex(colPosA[0], colPosA[1], colPosA[2]);
              vertex(colPosB[0], colPosB[1], colPosB[2]);
              vertex(colPosC[0], colPosC[1], colPosC[2]);
            endShape(CLOSE);
            
          faceCount++;
        }
      }    
      noLights();
    }
  }

  void drawCurves() {
    
    for (int y=0; y<graphHeight ;y++){
      for (int n=0; n<3; n+=2){
        if (makeCurve[n][y]==true){

          String[] tempFace = splitTokens(pixRowArray[y], "#"); 
          
          for (int x=0; x<tempFace.length ;x++){
            
            String[] tempPts = split(tempFace[x], ",");  
            String[] tempPtsAlt = split(tempFace[x], ",");  
            
            pushMatrix();  
              translate((width/6)+(width/3), height/2);
              rotateY(ROTX);
              rotateX(ROTY);
              scale(SCALE);
              float myPosX = ((float(tempPts[0]))+(float(tempPts[3]))+(float(tempPts[6])))/3;
              myPosX = map(myPosX, boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
              float myPosY = ((float(tempPts[1]))+(float(tempPts[4]))+(float(tempPts[7])))/3;
              myPosY = map(myPosY, boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
              float myPosZ = ((float(tempPts[2]))+(float(tempPts[5]))+(float(tempPts[8])))/3;
              myPosZ = map(myPosZ, boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
              float tempMyPosX = screenX(myPosX, myPosY, myPosZ);
              float tempMyPosY = screenY(myPosX, myPosY, myPosZ);  
            popMatrix();
            
            pushMatrix();  
              translate((width/6)+(n*(width/3)), height/2);
              rotateY(ROTX);
              rotateX(ROTY);
              scale(SCALE);  
              float posX = ((float(tempPtsAlt[0]))+(float(tempPtsAlt[3]))+(float(tempPtsAlt[6])))/3;
              posX = map(posX, boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
              float posY = ((float(tempPtsAlt[1]))+(float(tempPtsAlt[4]))+(float(tempPtsAlt[7])))/3;
              posY = map(posY, boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
              float posZ = ((float(tempPtsAlt[2]))+(float(tempPtsAlt[5]))+(float(tempPtsAlt[8])))/3;
              posZ = map(posZ, boundMax[0], boundMax[1], -displayBox/2, displayBox/2);       
              float tempPosX = screenX(posX, posY, posZ);
              float tempPosY = screenY(posX, posY, posZ); 
              float tempPosMapY = graphHeight - y + (screenGUI/2);              
            popMatrix();
            
            strokeWeight(5);
            noFill(); 
            stroke(255,5);
            
            if(pixRowStatus[y]==0){stroke(255,0,255,5);}
            if(pixRowStatus[y]==1){stroke(255,255,0,5);}
            if(pixRowStatus[y]==3){stroke(0,255,255,5);}
            if(pixRowStatus[y]==4){stroke(255,255,255,5);}
        
            if (n==0){ 
               bezier(tempMyPosX, tempMyPosY, tempPosX, tempPosY, tempPosX, tempPosY, 0, tempPosMapY);
               //line(tempMyPosX, tempMyPosY, 0, graphHeight - y + (screenGUI/2));
            } else {
               bezier(tempMyPosX, tempMyPosY, tempPosX, tempPosY, tempPosX, tempPosY, screenW, tempPosMapY);
               //line(tempMyPosX, tempMyPosY, screenW, graphHeight - y + (screenGUI/2));
            } 
            
          }  
        }
      }
    }
  }

  void displayBoundingBox() {
    
    strokeWeight(1/SCALE);
    stroke(255);
    noFill();
    box(displayBox);
    
    strokeWeight(1/SCALE);
    stroke(255,0,0);
    noFill();

    pushMatrix();
      box(xRange*scaleFactor, yRange*scaleFactor, zRange*scaleFactor);
    popMatrix();

  }
  
  void sortGeom(){
    
 //////CREATE A GRAPH OF FACES////////////////////////////////////////////////////////    
    for (int r=0; r<graphHeight; r++) {
      pixRowArray[r]="#";
    }
    
    ///CREATE TEXT FILE IN GRAPH STRUCTURE
//    println(index + " boundMax... " + boundMaxZ[0] + " - " + boundMaxZ[1]);
    for (int i=0; i<fList.size(); i++) { 
        String temp[] = split(fList.get(i), ",");
        float tempZVal = (float(temp[2]) + float(temp[5]) + float(temp[8]))/3; 
        int row = int(map(tempZVal, boundMaxZ[0], boundMaxZ[1], 0, graphHeight-1));
//        println("row... " + row);
        pixRowArray[row] = pixRowArray[row] + fList.get(i) +"#";
    }
    
    ///CLEAN UP TEXT FILE
    for (int i=0; i<graphHeight; i++){
      if (pixRowArray[i].length()>1){
        pixRowArray[i] = pixRowArray[i].substring(0, pixRowArray[i].length()-1);
        pixRowArray[i] = pixRowArray[i].substring(1);
      } else {
        pixRowArray[i] = "";
      } 
    }
    
//    saveStrings("data/loaded/pixGraph-" + index + "Face.txt", pixRowArray);
       
    ///CALCULATE MAXIMUM WIDTH OF GRAPH
    maxWidth=0;
    for (int i=0; i<graphHeight; i++){
      String temp[] = split(pixRowArray[i], "#"); 
      if ((temp.length)> maxWidth){maxWidth = temp.length;}
    }
    maxWidth = maxWidth*9;

    pgFace = createGraphics(maxWidth, graphHeight);
    pgFace.beginDraw();
    pgFace.background(0,0);
    
    for (int i=0; i<graphHeight; i++){
      
      int pixColCount=0;
      String tempFace[] = split(pixRowArray[i], "#");
      
      if (tempFace.length>2){
        
        for (int j=0; j<tempFace.length; j++){   
          String tempVert [] = split(tempFace[j], ",");
          for (int k=0; k<9; k++){
            float tempCol = float(tempVert[k]);
            color pixCol = int(tempCol);
            pgFace.set(pixColCount, graphHeight-i, pixCol);
            pixColCount++;
          }         
        }  
        
      }
      
    }
    
    pgFace.updatePixels();
    pgFace.endDraw();
    pgFace.save("data/loaded/pixGraph-" + index + "faceGrah.png");
    
//////CREATE A GRAPH OF VERICES////////////////////////////////////////////////////////

    
    for (int r=0; r<graphHeight; r++) {
      vertRowArray[r]="#";
    }
    
    ///CREATE TEXT FILE IN GRAPH STRUCTURE
    for (int i=0; i<pixList.size()-3; i+=3) { 
        int row = int(map(pixList.get(i+2), boundMaxZ[0], boundMaxZ[1], 0, graphHeight-1));
        vertRowArray[row] = vertRowArray[row] + pixList.get(i) +",";
        vertRowArray[row] = vertRowArray[row] + pixList.get(i+1) +",";
        vertRowArray[row] = vertRowArray[row] + pixList.get(i+2) +"#";
    }
    ///CLEAN UP TEXT FILE
    for (int i=0; i<graphHeight; i++){
      if (vertRowArray[i].length()>1){
        vertRowArray[i] = vertRowArray[i].substring(0, vertRowArray[i].length()-1);
        vertRowArray[i] = vertRowArray[i].substring(1);
      } else {
        vertRowArray[i] = "";
      } 
    }
    
//    saveStrings("data/loaded/pixGraph-" + index + "Vert.txt", vertRowArray);
    
    ///CALCULATE MAXIMUM WIDTH OF GRAPH
    maxWidth = 0;
    for (int i=0; i<graphHeight; i++){
      String temp[] = split(vertRowArray[i], "#"); 
      if ((temp.length)> maxWidth){maxWidth = temp.length;}
    }
    maxWidth = maxWidth*3;

    pgVert = createGraphics(maxWidth, graphHeight);
    pgVert.beginDraw();
    pgVert.background(0,0);
    
    for (int i=0; i<graphHeight; i++){
      
      int pixColCount=0;
      String tempVert[] = split(vertRowArray[i], "#");
      
      for (int j=0; j<tempVert.length; j++){   
        String tempCord [] = split(tempVert[j], ",");
        
        for (int k=0; k<tempCord.length; k++){
          float tempCol = float(tempCord[k]);
          color pixCol = int(tempCol);
          pgVert.set(pixColCount, graphHeight-i, pixCol);
          pixColCount++;
        }         
      }  
      
    }
    
    pgVert.updatePixels();
    pgVert.endDraw();
    pgVert.save("data/loaded/pixGraph-" + index + "vertGraph.png");    
  }
  

  
  void getBoundingBox(){
     
    boundX[0] = 16777216;
    boundX[1] = -16777216;
    boundY[0] = 16777216;
    boundY[1] = -16777216;
    boundZ[0] = 16777216;
    boundZ[1] = -16777216;

    int tempVert0[] = new int [3];
    int tempVert1[] = new int [3];
    int tempVert2[] = new int [3];
    
    for (int i=0; i<pixList.size(); i+=9) {
      
      if (pixList.get(i)<0) {
        
        tempVert0[0] = pixList.get(i);
        tempVert0[1] = pixList.get(i+1);
        tempVert0[2] = pixList.get(i+2);
        tempVert1[0] = pixList.get(i+3);
        tempVert1[1] = pixList.get(i+4);
        tempVert1[2] = pixList.get(i+5);        
        tempVert2[0] = pixList.get(i+6);
        tempVert2[1] = pixList.get(i+7);
        tempVert2[2] = pixList.get(i+8);        
      
        if (boundX[0]>tempVert0[0]){boundX[0] = tempVert0[0];}
        if (boundX[1]<tempVert0[0]){boundX[1] = tempVert0[0];}
        if (boundX[0]>tempVert1[0]){boundX[0] = tempVert1[0];}
        if (boundX[1]<tempVert1[0]){boundX[1] = tempVert1[0];}
        if (boundX[0]>tempVert2[0]){boundX[0] = tempVert2[0];}
        if (boundX[1]<tempVert2[0]){boundX[1] = tempVert2[0];}       
        
        if (boundY[0]>tempVert0[1]){boundY[0] = tempVert0[1];}
        if (boundY[1]<tempVert0[1]){boundY[1] = tempVert0[1];}
        if (boundY[0]>tempVert1[1]){boundY[0] = tempVert1[1];}
        if (boundY[1]<tempVert1[1]){boundY[1] = tempVert1[1];}
        if (boundY[0]>tempVert2[1]){boundY[0] = tempVert2[1];}
        if (boundY[1]<tempVert2[1]){boundY[1] = tempVert2[1];} 
        
        if (boundZ[0]>tempVert0[2]){boundZ[0] = tempVert0[2];}
        if (boundZ[1]<tempVert0[2]){boundZ[1] = tempVert0[2];}
        if (boundZ[0]>tempVert1[2]){boundZ[0] = tempVert1[2];}
        if (boundZ[1]<tempVert1[2]){boundZ[1] = tempVert1[2];}
        if (boundZ[0]>tempVert2[2]){boundZ[0] = tempVert2[2];}
        if (boundZ[1]<tempVert2[2]){boundZ[1] = tempVert2[2];} 
    
      }
    }
    
    xRange = boundX[1]-boundX[0];
    yRange = boundY[1]-boundY[0];
    zRange = boundZ[1]-boundZ[0];
    
    if (xRange>yRange){
      if (xRange>zRange){
        boundMax[0]=boundX[0];
        boundMax[1]=boundX[1];
      }else{
        boundMax[0]=boundZ[0];
        boundMax[1]=boundZ[1];
      }
    } else {
      if (yRange>zRange) { 
        boundMax[0]=boundY[0];
        boundMax[1]=boundY[1];
      } else {
        boundMax[0]=boundZ[0];
        boundMax[1]=boundZ[1];
      }
    }
    
    if (boundZ[0]<boundMaxZ[0]){boundMaxZ[0] = boundZ[0];}
    if (boundZ[1]>boundMaxZ[1]){boundMaxZ[1] = boundZ[1];}
    
    scaleFactor = displayBox/(boundMax[1]-boundMax[0]);

//    println(xRange + " = " +  boundX[0] + " - " + boundX[1]);
//    println(yRange + " = " +  boundY[0] + " - " + boundY[1]);
//    println(zRange + " = " +  boundZ[0] + " - " + boundZ[1]);
//    println(boundMax[0] + "-" +  boundMax[1]);
//    println(scaleFactor);
//    println(boundMaxZ[0] + " > " + boundMaxZ[1]);
    
  }
  
  void getGeometry(){

    float aX, aY, aZ;
    float bX, bY, bZ;
    float cX, cY, cZ;
    
    String tempPts;
    
    pixList.clear();
    fList.clear();
   

    for (int i=0; i<pixData.pixels.length; i+=9) {
      if (pixData.pixels[i]<0) {
        
        pixList.add(pixData.pixels[i]);
        pixList.add(pixData.pixels[i+1]);
        pixList.add(pixData.pixels[i+2]);
        pixList.add(pixData.pixels[i+3]);
        pixList.add(pixData.pixels[i+4]);
        pixList.add(pixData.pixels[i+5]);
        pixList.add(pixData.pixels[i+6]);
        pixList.add(pixData.pixels[i+7]);
        pixList.add(pixData.pixels[i+8]);
        
//        int rowVal = i/pixData.width;
        
//////////////////////////////COLLECT THE ABSOLUTE GEOMETRY OF THE OBJECT
        aX = pixData.pixels[i];
        aY = pixData.pixels[i+1];
        aZ = pixData.pixels[i+2];

        bX = pixData.pixels[i+3];
        bY = pixData.pixels[i+4];
        bZ = pixData.pixels[i+5];

        cX = pixData.pixels[i+6];
        cY = pixData.pixels[i+7];
        cZ = pixData.pixels[i+8];
        
        tempPts = str(aX) + "," + str(aY) + "," + str(aZ) + "," + str(bX) + "," + str(bY) + "," + str(bZ) + "," + str(cX) + "," + str(cY) + "," + str(cZ);
        fList.add(tempPts);
        
      }  
      
    }
    
//    println(imageList.get(liveIndex) + " loaded");

  }
   
  void makeGraph() {   
    
    pg = createGraphics(width, screenH);
    pg.beginDraw();
    pg.background(0,0);
    pg.strokeWeight(1);
    pg.stroke(255);
    
    for (int y=0; y<graphHeight ;y++){;
    
      int pixY = graphHeight-y + (screenGUI/2);
      String[] tempCol = split(pixRowArray[y], "#");
      
      if(tempCol.length>1){
        if (index==0){ pg.line(0, pixY, (tempCol.length*9), pixY);}
        if (index==1){ pg.line((screenW/2)-((tempCol.length*9)/2), pixY, (screenW/2)+(tempCol.length/2*9), pixY);}
        if (index==2){ pg.line(screenW, pixY, screenW-(tempCol.length*9)-1, pixY);}
      }
      
    }
      
    pg.endDraw();
    pg.save("data/loaded/pixGraph-"+index+"-loaded.png");

  }
  
  void getPixData(){
    pixData = loadImage(imageList.get(liveIndex));
    pixData.loadPixels();
  }
  
}
  
//////////////////////////////////////////////////////////////////
void intializePixMod() {
//  println("initializePixMod");
  for (int i=0; i<3; i++) {
    if (i==1) {
      pixMod[i] = new pixModel(i, false, true);
    } else {
      pixMod[i] = new pixModel(i, true, false);
    }
  }
}

