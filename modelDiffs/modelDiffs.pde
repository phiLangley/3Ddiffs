int screenH = 512;
int screenGUI = 80;
int guiDim = 20;
int screenW = 1200;

pixModel[] pixMod = new pixModel[3];
buttonSlider[] buttonSlide = new buttonSlider[3];
buttonClicker[] buttonClick = new buttonClicker[3];

int colRangeMax = 16777215;
int precisionVal = 1;
int graphHeight = 512;
int displayBox = 16777216;

float boundMaxZ[] = new float[2];

boolean getPixData=false;
boolean loadNewPix=true;

String path;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setup(){

  noLoop();
  size(screenW, screenH+screenGUI, OPENGL);
  
  path = savePath("");
  println(path);
  
  getFileList();
  initializeSliders();
  initializeButtons();
  intializePixMod();

}

void draw(){
  
  checkUpdates();
  
  background(0);
  
/////////////draw the buttons
  for (int i=0; i<3; i++){
    buttonSlide[i].display();
    buttonClick[i].display();
  }
  
/////////////dispaly the graph, according to the button controls
  for (int i=0; i<3; i++){
    if (pixMod[i].dispGraph==true){
      image(pixMod[i].pg, 0,0);
    }
  }
  
/////////////dispaly the model, according to the button controls  
  for (int i=0; i<3; i++){
    pushMatrix();
      translate((width/6)+(i*(width/3)), height/2);
      rotateY(ROTX);
      rotateX(ROTY);
      scale(SCALE);

      pixMod[i].displayDiffMesh();
      pixMod[i].displayBoundingBox();
   
    popMatrix();
  }  
  

/////////////middle model draws the differnece lines to the previous and future versions
  pixMod[1].drawCurves();
  
}





